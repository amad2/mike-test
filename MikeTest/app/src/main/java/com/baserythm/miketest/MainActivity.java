package com.baserythm.miketest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableInt;
import androidx.room.Room;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.baserythm.miketest.Database.AppDatabase;
import com.baserythm.miketest.Database.RandomData;
import com.baserythm.miketest.Database.RandomDataDao;
import com.baserythm.miketest.Database.UserLocation;
import com.baserythm.miketest.Database.UserLocationDao;
import com.baserythm.miketest.Services.LocationService;
import com.baserythm.miketest.Services.LocationUploadService;
import com.baserythm.miketest.Services.RandomDataUploadService;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    String TAG = "main_act";
    int permissionRequestCode = 11;

    //Count variables
    int recordCount = 0;

    //Views
    TextView txtPendingDataCount,txtPendingLocationCount;
    ProgressBar pbar;

    //Database Instance
    AppDatabase db;
    UserLocationDao userLocationDao;
    RandomDataDao randomDataDao;

    //Location Object
    private FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;
    LocationCallback locationCallback;


    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Initialize widgets
        widgetsInit();

        //Lets check for location permission
        if (
                ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED
        ){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION},permissionRequestCode);
        }
        //Initialize database
        db = AppDatabase.getInstance(MainActivity.this);
        userLocationDao = db.userLocationDao();
        randomDataDao = db.randomDataDao();

        txtPendingDataCount.setText(""+ randomDataDao.recordsCount());

        if (!isMyServiceRunning(LocationService.class)){
            startService();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activty,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.download_data:
                Toast.makeText(this, "Downloading data", Toast.LENGTH_SHORT).show();
                pbar.setVisibility(View.VISIBLE);
                AndroidNetworking.get("https://api.test.datacite.org/providers/caltech/dois?page[size]=1000")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try{
                                    JSONArray data = response.getJSONArray("data");
                                    for (int i = 0; i < data.length(); i++){
                                        JSONObject obj = data.getJSONObject(i);
                                        String id = obj.getString("id");
                                        final RandomData rd = new RandomData();
                                        rd.title = id;
                                        randomDataDao.insertNewData(rd);
                                        txtPendingDataCount.setText(""+ randomDataDao.recordsCount());
                                    }
                                    pbar.setVisibility(View.GONE);
                                }
                                catch (JSONException ex){
                                    Log.e(TAG, "onResponse: " + ex.getMessage());
                                    pbar.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Log.e(TAG, "onError: ", anError);
                                pbar.setVisibility(View.GONE);
                            }
                        });
                return true;
            case R.id.start_upload_service:
                if (!isMyServiceRunning(RandomDataUploadService.class)){
                    startDataUploadService();
                }
                return true;
            case R.id.stop_upload_service:
                if (isMyServiceRunning(RandomDataUploadService.class)){
                    stopDataUploadService();
                }
                return true;
            case R.id.start_location_upload:
                if (!isMyServiceRunning(LocationUploadService.class)){
                    startLocationUploadService();
                }
                return true;
            case R.id.stop_location_upload:
                if (isMyServiceRunning(LocationUploadService.class)){
                    stopLocationUploadService();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void startService() {
        Intent serviceIntent = new Intent(this, LocationService.class);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public void startLocationUploadService() {
        Intent serviceIntent = new Intent(this, LocationUploadService.class);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public void stopLocationUploadService() {
        Intent serviceIntent = new Intent(this, LocationUploadService.class);
        stopService(serviceIntent);
    }

    public void startDataUploadService() {
        Intent serviceIntent = new Intent(this, RandomDataUploadService.class);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public void stopDataUploadService() {
        Intent serviceIntent = new Intent(this, RandomDataUploadService.class);
        stopService(serviceIntent);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    //View click listeners
    public void onTxtLocationCountClick(View v){
        startActivity(new Intent(MainActivity.this, ListActivity.class).putExtra("flag", 1));
    }

    public void onRandomDataCountClick(View v){
        startActivity(new Intent(MainActivity.this, ListActivity.class).putExtra("flag", 0));
    }

    private void widgetsInit(){
        txtPendingDataCount = findViewById(R.id.txtPendingDataCount);
        txtPendingLocationCount = findViewById(R.id.txtPendingLocationCount);
        pbar = findViewById(R.id.pbar);
    }

    //Permission listeners
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == permissionRequestCode){
            if (grantResults.length > 0){
                Toast.makeText(this, "Welcome", Toast.LENGTH_SHORT).show();
            }  else {
                Toast.makeText(this, "Permission Error", Toast.LENGTH_SHORT).show();
                MainActivity.this.finish();
            }
        }
    }

    @Override
    protected void onStart() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(LocationService.ACTION1);
        registerReceiver(broadcastReceiver, intentFilter);

        IntentFilter intentFilter1 = new IntentFilter();
        intentFilter1.addAction(RandomDataUploadService.ACTION2);
        registerReceiver(broadcastReceiverRandomdata, intentFilter1);

        txtPendingLocationCount.setText(""+userLocationDao.recordsCount());
        txtPendingDataCount.setText(""+randomDataDao.recordsCount());
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(broadcastReceiver);
        unregisterReceiver(broadcastReceiverRandomdata);
        super.onStop();
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int s1 = intent.getIntExtra("LOCATION-SERVICE-COUNT", 0);
            txtPendingLocationCount.setText(""+ s1);
        }
    };

    BroadcastReceiver broadcastReceiverRandomdata = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int s1 = intent.getIntExtra("RANDOM-SERVICE-COUNT", 0);
            txtPendingDataCount.setText(""+ s1);
        }
    };
}