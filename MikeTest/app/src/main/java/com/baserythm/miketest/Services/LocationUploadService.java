package com.baserythm.miketest.Services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.baserythm.miketest.App;
import com.baserythm.miketest.Database.AppDatabase;
import com.baserythm.miketest.Database.UserLocation;
import com.baserythm.miketest.Database.UserLocationDao;
import com.baserythm.miketest.MainActivity;
import com.baserythm.miketest.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class LocationUploadService extends Service {

    public final static String ACTION1 = "ACTION1";

    //Database Instance
    AppDatabase db;
    UserLocationDao userLocationDao;
    List<UserLocation> ulist;
    Timer t;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, App.CHANNEL_ID)
                .setContentTitle("Location Service")
                .setContentText("Mike test is backing up location data")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(2, notification);

        db = AppDatabase.getInstance(LocationUploadService.this);
        userLocationDao = db.userLocationDao();
        ulist = new ArrayList<>();
        if (ulist.size() == 0){
            t = new Timer();
            t.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    ulist = userLocationDao.getAllLocations();
                    Log.d("uploadService", "run: ulist size" + ulist.size());
                    for (int i = 0; i < ulist.size(); i++){
                        final UserLocation ul = ulist.get(i);
                        AndroidNetworking.post("http://192.168.18.48:8888/miketest/location.php")
                                .addBodyParameter("id", String.valueOf(ul.id))
                                .addBodyParameter("lat", String.valueOf(ul.latitude))
                                .addBodyParameter("lng", String.valueOf(ul.longitude))
                                .setPriority(Priority.HIGH)
                                .build()
                                .getAsString(new StringRequestListener() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("uploadService", "onResponse: " + response);
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                userLocationDao.deleteUserLocation(ul);
                                                Intent intent = new Intent();
                                                intent.setAction(ACTION1);
                                                intent.putExtra("LOCATION-SERVICE-COUNT", userLocationDao.recordsCount());
                                                sendBroadcast(intent);
                                            }
                                        }).start();
                                        ulist.clear();
                                    }

                                    @Override
                                    public void onError(ANError anError) {
                                        Log.d("uploadService", "onResponseError: " + anError.getErrorDetail());
                                    }
                                });
                    }
                }
            },0,1000);

        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        t.cancel();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
