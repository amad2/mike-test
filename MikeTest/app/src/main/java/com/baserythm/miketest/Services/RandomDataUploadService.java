package com.baserythm.miketest.Services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.baserythm.miketest.App;
import com.baserythm.miketest.Database.AppDatabase;
import com.baserythm.miketest.Database.RandomData;
import com.baserythm.miketest.Database.RandomDataDao;
import com.baserythm.miketest.Database.UserLocation;
import com.baserythm.miketest.Database.UserLocationDao;
import com.baserythm.miketest.MainActivity;
import com.baserythm.miketest.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class RandomDataUploadService extends Service {
    public final static String ACTION2 = "ACTION2";

    //Database Instance
    AppDatabase db;
    RandomDataDao randomDataDao;
    List<RandomData> ulist;
    Timer t;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, App.CHANNEL_ID)
                .setContentTitle("Data Service")
                .setContentText("Mike test is backing up data")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(3, notification);

        db = AppDatabase.getInstance(RandomDataUploadService.this);
        randomDataDao = db.randomDataDao();
        ulist = new ArrayList<>();

        if (ulist.size() == 0){
            t = new Timer();
            t.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    ulist = randomDataDao.getAllData();
                    Log.d("uploadService", "run: ulist size" + ulist.size());
                    for (int i = 0; i < ulist.size(); i++){
                        final RandomData ul = ulist.get(i);
                        AndroidNetworking.post("http://192.168.18.48:8888/miketest/randomdata.php")
                                .addBodyParameter("id", String.valueOf(ul.id))
                                .addBodyParameter("name", String.valueOf(ul.title))
                                .setPriority(Priority.HIGH)
                                .build()
                                .getAsString(new StringRequestListener() {
                                    @Override
                                    public void onResponse(String response) {
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                randomDataDao.deleteData(ul);

                                                Intent intent = new Intent();
                                                intent.setAction(ACTION2);
                                                intent.putExtra("RANDOM-SERVICE-COUNT", randomDataDao.recordsCount());
                                                sendBroadcast(intent);
                                            }
                                        }).start();
                                        ulist.clear();
                                    }

                                    @Override
                                    public void onError(ANError anError) {
                                        Log.d("uploadService", "onResponseError: " + anError.getErrorDetail());
                                    }
                                });
                    }
                }
            },0,1000);

        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        t.cancel();
    }
}
