package com.baserythm.miketest.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {UserLocation.class,RandomData.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static final String DB_NAME = "db-mike-test";
    private static volatile AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    protected AppDatabase() {};

    private static AppDatabase create(final Context context) {
//        final Migration MIGRATION_1_2 = new Migration(1, 2) {
//
//            @Override
//            public void migrate(SupportSQLiteDatabase database) {
//                // Create the new table
//                database.execSQL("CREATE TABLE user_location_new (id int, lat double, PRIMARY KEY(id))");
//                // Copy the data
//                database.execSQL("INSERT INTO user_location_new (id, lat) SELECT id, lat FROM userlocation");
//                        // Remove the old table
//                database.execSQL("DROP TABLE userlocation");
//                // Change the table name to the correct one
//                database.execSQL("ALTER TABLE user_location_new RENAME TO userlocation");
//            }
//
//        };
        return Room.databaseBuilder(context, AppDatabase.class, DB_NAME).allowMainThreadQueries().build();
    }
    public abstract UserLocationDao userLocationDao();
    public abstract RandomDataDao randomDataDao();
}
