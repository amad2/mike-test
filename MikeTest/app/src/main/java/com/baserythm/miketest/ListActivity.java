package com.baserythm.miketest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.os.Bundle;

import com.baserythm.miketest.Adapters.OurCustomListAdapter;
import com.baserythm.miketest.Database.AppDatabase;
import com.baserythm.miketest.Database.RandomData;
import com.baserythm.miketest.Database.RandomDataDao;
import com.baserythm.miketest.Database.UserLocation;
import com.baserythm.miketest.Database.UserLocationDao;

import java.util.List;

public class ListActivity extends AppCompatActivity {

    //Database Instance
    AppDatabase db;
    UserLocationDao userLocationDao;
    RandomDataDao randomDataDao;

    //RecyclerView
    RecyclerView rview;
    OurCustomListAdapter customListAdapter;
    String[] dataset;
    List<UserLocation> usl;
    List<RandomData> rnd;

    //flag
    int flag = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        //Init widgets
        widgets();

        //
        flag = getIntent().getExtras().getInt("flag",2);

        //Init database
        db = AppDatabase.getInstance(ListActivity.this);
        userLocationDao = db.userLocationDao();
        randomDataDao = db.randomDataDao();

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (flag == 0){
                    rnd = randomDataDao.getAllData();
                    dataset = new String[rnd.size()];
                    for (int i = 0; i < rnd.size(); i++){
                        dataset[i] = "ID: " + rnd.get(i).id + "\nData: " + rnd.get(i).title;
                    }
                }
                else if(flag == 1){
                    usl = userLocationDao.getAllLocations();
                    dataset = new String[usl.size()];
                    for (int i = 0; i < usl.size(); i++){
                        dataset[i] = "ID: " + usl.get(i).id + "\nLat: " + usl.get(i).latitude + "\nLng: " + usl.get(i).longitude;
                    }
                }
                initRecyclerView();
            }
        }).start();
    }

    void widgets(){
        rview = findViewById(R.id.rView);
    }

    void initRecyclerView(){
        customListAdapter = new OurCustomListAdapter(dataset);
        rview.setAdapter(customListAdapter);
        rview.setLayoutManager(new LinearLayoutManager(this));
    }
}