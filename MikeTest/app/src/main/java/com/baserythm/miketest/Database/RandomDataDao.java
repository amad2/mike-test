package com.baserythm.miketest.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface RandomDataDao {
    @Query("SELECT * FROM randomdata")
    List<RandomData> getAllData();

    @Query("SELECT * FROM randomdata LIMIT 1")
    RandomData singleData();

    @Query("SELECT COUNT(*) FROM randomdata")
    int recordsCount();

    @Insert
    void insertAll(RandomData... randomData);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertNewData(RandomData... randomData);

    @Delete
    public void deleteData(RandomData... randomData);
}
