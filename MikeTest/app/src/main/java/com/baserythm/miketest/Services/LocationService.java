package com.baserythm.miketest.Services;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.baserythm.miketest.App;
import com.baserythm.miketest.Database.AppDatabase;
import com.baserythm.miketest.Database.UserLocation;
import com.baserythm.miketest.Database.UserLocationDao;
import com.baserythm.miketest.MainActivity;
import com.baserythm.miketest.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class LocationService extends Service {

    public final static String ACTION1 = "ACTION1";

    //Database Instance
    AppDatabase db;
    UserLocationDao userLocationDao;

    //Location Object
    private FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;
    LocationCallback locationCallback;

    int globalCount = 0;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, App.CHANNEL_ID)
                .setContentTitle("Location Service")
                .setContentText("Mike test is using your location")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);

        //Start fetching location
        //Initialize database
        db = AppDatabase.getInstance(LocationService.this);
        userLocationDao = db.userLocationDao();
        //Initialize Location Object
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        //Location callback
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
                UserLocation userLocation = new UserLocation();
                userLocation.latitude = locationResult.getLastLocation().getLatitude();
                userLocation.longitude = locationResult.getLastLocation().getLongitude();

                new mytask().execute(userLocation);
            }
        };

        //Getting location
        locationRequest = new LocationRequest();
        locationRequest.setInterval(60 * 1000); //Getting location every minute
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        return START_NOT_STICKY;
    }

    public class mytask extends AsyncTask<UserLocation,Integer,Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(UserLocation... userLocations) {
            userLocationDao.insertUserLocation(userLocations);
            globalCount = userLocationDao.recordsCount();
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Intent intent = new Intent();
            intent.setAction(ACTION1);
            intent.putExtra("LOCATION-SERVICE-COUNT", globalCount);
            sendBroadcast(intent);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
