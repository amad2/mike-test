package com.baserythm.miketest.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserLocationDao {
    @Query("SELECT * FROM userlocation")
    List<UserLocation> getAllLocations();

    @Query("SELECT * FROM userlocation LIMIT 1")
    UserLocation singleLocation();

    @Query("SELECT COUNT(*) FROM userlocation")
    int recordsCount();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertUserLocation(UserLocation... userLocation);

    @Delete
    public void deleteUserLocation(UserLocation... userLocation);
}
